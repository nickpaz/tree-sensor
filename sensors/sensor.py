from abc import ABC, abstractmethod

class Sensor(ABC):

    # get one sensor reading (as 420 data)
    @abstractmethod
    def get_reading(self):
        pass

    # real range(0 to 500) -> 420 range(0.004 to 0.02)
    @abstractmethod
    def convert_to_420(self, reading):
        m = 0.000032
        b = 0.004
        y = m * reading + b
        return y
