from sensor import Sensor
import Adafruit_DHT
import random
# setup

# sudo apt-get update
# sudo apt-get install python3-pip
# sudo python3 -m pip install --upgrade pip setuptools wheel
# sudo pip3 install Adafruit_DHT

# temp-humidity sensor
class DHT22(Sensor):

    def __init__(self, pin=4):
        self.pin = pin
        self.sensor = Adafruit_DHT.DHT22

    def get_reading(self):
        readings = []
        humidity, temperature = Adafruit_DHT.read_retry(self.sensor, self.pin)
        if humidity is not None and temperature is not None:
            readings = (self.convert_to_420(humidity), self.convert_to_420(temperature))
            temperature = temperature * 1.8 + 32 
            readings = (humidity, temperature)

        else: 
            readings = (0, 0)
        return readings

    def convert_to_420(self,reading):
        return super().convert_to_420(reading)


if __name__ == "__main__":
    sensor = DHT22()
    print(sensor.get_reading())