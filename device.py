#!/usr/bin/env python3

import os, time, json, requests
from collections import OrderedDict
from dotenv import load_dotenv
from sensors.dht22 import DHT22
load_dotenv()

DEVICE_ID = os.environ['DEVICE_ID']
DEVICE_AUTH = os.environ['DEVICE_AUTH']
RMQ_SERVER = os.environ['RMQ_SERVER']
RMQ_PORT = os.environ['RMQ_PORT']
MODIFIED_JSON = "./json/%s.json" % DEVICE_ID
BASE_JSON = "./json/data.json"
AVMSA = './avmsavdc -s %s -p %s -j %s -i %s -a %s' % (
    RMQ_SERVER,
    RMQ_PORT,
    MODIFIED_JSON,
    DEVICE_ID,
    DEVICE_AUTH
)

def get_sensor_readings():

    readings = [0]*7

    # ===== ===== # v IMPLEMENT v # ===== ===== # ===== ===== # v IMPLEMENT v # ===== ===== #

    temp_humidity_sensor = DHT22()

    humidity, temp = temp_humidity_sensor.get_reading()

    print((
        "temp: %f\n"
        "humidity: %f\n"
    ) % (temp, humidity))

    readings[0] = temp
    readings[1] = humidity

    # ===== ===== # ^ IMPLEMENT ^ # ===== ===== # ===== ===== # ^ IMPLEMENT ^ # ===== ===== #

    return readings

if __name__ == "__main__":

    # SEND DATA TO AVIMESA

    # copy base json data over into dict
    with open(BASE_JSON, "r") as read_file:
        data = json.load(read_file, object_pairs_hook=OrderedDict)
    data['dev']['dev_id'] = DEVICE_ID
    data['dts'] = time.time()

    # set channel values
    readings = get_sensor_readings()
    for i, reading in enumerate(readings):
        data['dev']['chans'][i]['ch_data'][0]['val'] = reading

    # save data to json
    with open(MODIFIED_JSON, "w") as write_file:
        json.dump(data, write_file)

    # send data to avimesa
    os.system(AVMSA)

    print('\n')
