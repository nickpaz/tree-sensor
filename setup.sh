apt update
apt install -y python3-pip
python3 -m pip install --upgrade pip setuptools wheel
pip3 install -r requirements.txt

echo '>>> Creating cronjob'

# rerouting cron output to /dev/null because it emails the user every time
JOB='* * * * * cd /home/pi/moto-device; ./device.py >> /home/pi/moto-cron.log 2>&1'
FINDJOB=$(crontab -l | grep -F "$JOB")
if [ -z "$FINDJOB" ]
then
  (crontab -l 2>/dev/null; echo "$JOB") | crontab -
fi